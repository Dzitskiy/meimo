﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Meimo.Services.EmailNotifier.Api.Models.Api;
using Meimo.Services.EmailNotifier.Core.Abstractions.Repositories;
using Meimo.Services.EmailNotifier.Core.Domain.Notification;
using Microsoft.AspNetCore.Mvc;

namespace Meimo.Services.EmailNotifier.Api.Controllers
{
    /// <summary>
    /// Управление шаблонами электронных писем
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmailTemplateController: ControllerBase
    {
        private readonly IMapper _mapper;
        private IRepository<EmailTemplate> _emailTemplateRepository;
        
        public EmailTemplateController(IMapper mapper,  IRepository<EmailTemplate> emailTemplateRepository)
        {
            _mapper = mapper;
            _emailTemplateRepository = emailTemplateRepository;
        }
        
        /// <summary>
        /// Получение всех шаблонов
        /// </summary>
        /// <returns>Список шаблонов</returns>
        [HttpGet]
        public async Task<IEnumerable<EmailTemplateShortResponse>> GetEmailTemplatesAsync()
        {
            var emailTemplates = await _emailTemplateRepository.GetAllAsync();

            var emailTemplatesModelList = emailTemplates
                .Select(_mapper.Map<EmailTemplateShortResponse>);

            return emailTemplatesModelList;
        }
        
        /// <summary>
        /// Добавление нового шаблона
        /// </summary>
        /// <param name="request">Параметры для нового шаблона</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateEmailTemplateAsync(CreateEmailTemplateRequest request)
        {
            await _emailTemplateRepository.AddAsync(_mapper.Map<EmailTemplate>(request));
            return Ok();
        }

        /// <summary>
        /// Обновление шаблона
        /// </summary>
        /// <param name="request">Параметры для обновлённого шаблона</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmailTemplateAsync(UpdateEmailTemplateRequest request)
        {
            var emailTemplate = await _emailTemplateRepository.GetByIdAsync(request.Id);

            if (emailTemplate == null)
                return NotFound();
            
            await _emailTemplateRepository.UpdateAsync(_mapper.Map<EmailTemplate>(request));
            return Ok();
        }
        
        /// <summary>
        /// Получение подробной информации по клиенту
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <returns>Информация по клиенту</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<EmailTemplateResponse>> GetEmailTemplateAsync(Guid id)
        {
            var emailTemplate = await _emailTemplateRepository.GetByIdAsync(id);

            if (emailTemplate == null)
                return NotFound();

            return _mapper.Map<EmailTemplateResponse>(emailTemplate);
        }
    }
}