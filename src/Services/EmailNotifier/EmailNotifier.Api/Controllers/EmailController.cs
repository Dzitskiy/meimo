﻿using System.Threading.Tasks;
using AutoMapper;
using Meimo.Services.EmailNotifier.Api.Models.Api;
using Meimo.Services.EmailNotifier.Api.Models.Service;
using Meimo.Services.EmailNotifier.Api.Services;
using Meimo.Services.EmailNotifier.Core.Infrastructure.Logging;
using Microsoft.AspNetCore.Mvc;

namespace Meimo.Services.EmailNotifier.Api.Controllers
{
    /// <summary>
    /// Отправка электронных писем
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class SendController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ISendEmailService _sendEmailService;
        private readonly ITraceLogger _traceLogger;

        public SendController(IMapper mapper, ISendEmailService sendEmailService, ITraceLogger traceLogger)
        {
            _mapper = mapper;
            _sendEmailService = sendEmailService;
            _traceLogger = traceLogger;
        }

        /// <summary>
        /// Отправка сообщения
        /// </summary>
        /// <param name="request">Параметры сообщения</param>
        /// <returns>Результат выполнения</returns>
        [HttpPost("SendEmail")]        
        public async Task<IActionResult> SendEmail(SendEmailRequest request)
        {
            var tracedObject = new TracedObject<SendEmailRequest>(request);
            _traceLogger.TraceLog(string.Format("{0}.{1}",nameof(SendController), nameof(SendEmail)), tracedObject);
            await _sendEmailService.SendEmailAsync(_mapper.Map<TracedObject<SendingEmail>>(tracedObject));
            return Ok();
        }
        
        /// <summary>
        /// Отправка сообщения на основе шаблона
        /// </summary>
        /// <param name="request">Параметры сообщения</param>
        /// <returns>Результат выполнения</returns>
        [HttpPost("SendEmailTemplate")]        
        public async Task<IActionResult> SendEmailTemplate(TracedObject<SendEmailTemplateRequest> request)
        {
            _traceLogger.TraceLog(string.Format("{0}.{1}",nameof(SendController), nameof(SendEmailTemplate)), request);
            await _sendEmailService.SendEmailTemplateAsync(_mapper.Map<TracedObject<SendingEmailTemplate>>(request));
            
            return Ok();
        }
    }
}