﻿namespace Meimo.Services.EmailNotifier.Api.Models.Service
{
    public class SendingEmail
    {
        public string From { get; set; }
        public string Cc { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}