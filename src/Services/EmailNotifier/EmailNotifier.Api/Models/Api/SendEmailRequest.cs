﻿namespace Meimo.Services.EmailNotifier.Api.Models.Api
{
    /// <summary>
    /// Письмо для отправки
    /// </summary>
    public class SendEmailRequest
    {
        /// <summary>
        /// От кого
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// Копия
        /// </summary>
        public string Cc { get; set; }

        /// <summary>
        /// Кому
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// Тема письма
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Текст письма
        /// </summary>
        public string Body { get; set; }
    }
}