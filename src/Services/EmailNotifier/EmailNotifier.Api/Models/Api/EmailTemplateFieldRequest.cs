﻿namespace Meimo.Services.EmailNotifier.Api.Models.Api
{
    /// <summary>
    /// Стуктура запроса на создание поля-метки
    /// </summary>
    public class EmailTemplateFieldRequest
    {
        /// <summary>
        /// Ключ поля-метки
        /// </summary>
        public string Key { get; set; }
    }
}