﻿using System.Collections.Generic;

namespace Meimo.Services.EmailNotifier.Api.Models.Api
{
    /// <summary>
    /// Стуктура запроса на создание шаблона
    /// </summary>
    public class CreateEmailTemplateRequest
    {
        /// <summary>
        /// Название шаблона
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Текст сообщения с полями-метками для замены
        /// </summary>
        public string TemplateText { get; set;}
        
        /// <summary>
        /// Список название полей-меток для замены
        /// </summary>
        public List<EmailTemplateFieldRequest> TemplateFields { get; set; }
    }
}