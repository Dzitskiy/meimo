﻿using System.Collections.Generic;
using System.Linq;
using Meimo.Services.EmailNotifier.Api.Infrastructure.Exceptions;
using Meimo.Services.EmailNotifier.Api.Models.Service;
using Meimo.Services.EmailNotifier.Core.Domain.Notification;

namespace Meimo.Services.EmailNotifier.Api.Services
{
    public class EmailTemplateBodyBuilder
    {
        private EmailTemplate _emailTemplate;
        private List<SendingEmailTemplateParameter> _emailTemplateParameters;

        public EmailTemplateBodyBuilder(EmailTemplate emailTemplate, List<SendingEmailTemplateParameter> emailTemplateParameters)
        {
            _emailTemplate = emailTemplate;
            _emailTemplateParameters = emailTemplateParameters;
        }

        public string Build()
        {
            string builtBody = _emailTemplate.TemplateText;
            foreach (var emailTemplateParameter in _emailTemplateParameters)
            {
                if (_emailTemplate.TemplateFields.All(p => p.Key != emailTemplateParameter.Key))
                {
                    throw new EmailNotifierException(
                        $"В шаблоне с идентификатором '{_emailTemplate.Id}' не найден параметр: '{emailTemplateParameter.Key}'");
                }

                builtBody = builtBody.Replace(emailTemplateParameter.Key, emailTemplateParameter.Value);
            }

            return builtBody;
        }
    }
}