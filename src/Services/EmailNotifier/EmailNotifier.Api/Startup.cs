using AutoMapper;
using Meimo.Services.EmailNotifier.Api.Infrastructure.AutoMapper;
using Meimo.Services.EmailNotifier.Api.Infrastructure.Filters;
using Meimo.Services.EmailNotifier.Api.Services;
using Meimo.Services.EmailNotifier.Core.Abstractions.Repositories;
using Meimo.Services.EmailNotifier.Core.Domain.Notification;
using Meimo.Services.EmailNotifier.Core.Infrastructure.AutoMapper;
using Meimo.Services.EmailNotifier.Core.Infrastructure.Logging;
using Meimo.Services.EmailNotifier.DataAccess.Data;
using Meimo.Services.EmailNotifier.DataAccess.Repositories;
using Meimo.Services.EmailNotifier.Infrastructure;
using Meimo.Services.EmailNotifier.Infrastructure.Queues;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace Meimo.Services.EmailNotifier.Api
{
    public class Startup
    {
        private const string OpenApiTitle = "Meimo Email notifier - HTTP API";
        private const string OpenApiVersion = "1.0";
        private const string RabbitMqConfigSection = "RabbitMq";
        private const string ConfigConnectionString = "sqlConnectionString";

        private IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var serviceClientSettingsConfig = Configuration.GetSection(RabbitMqConfigSection);
            services.Configure<RabbitMqConfiguration>(serviceClientSettingsConfig);
            
            services.AddAutoMapper(typeof(EmailMapperProfile));
            services.AddAutoMapper(typeof(DomainModelProfile));
            
            services.AddControllers(options =>
            {
                options.Filters.Add(typeof(HttpGlobalExceptionFilter));
            });
            services.AddSingleton<ITraceLogger, TraceLogger>();
            services.AddDbContext<EmailDataContext>(p=>
                p.UseNpgsql(Configuration.GetConnectionString(ConfigConnectionString)));
            services.AddSingleton(Log.Logger);
            services.AddSingleton<ISendEmailQueue, SendEmailQueue>();
            services.AddTransient<ISendEmailService, SendEmailService>();
            services.AddTransient<IRepository<EmailTemplate>, EfEmailTemplateRepository>();
            
            services.AddOpenApiDocument(options =>
            {
                options.Title = OpenApiTitle;
                options.Version = OpenApiVersion;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, EmailDataContext dataContext)
        {
            dataContext.Database.Migrate();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            
            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}