﻿using System;

namespace Meimo.Services.EmailNotifier.Api.Infrastructure.Exceptions
{
    public class EmailNotifierException: Exception
    {
            public EmailNotifierException()
            { }

            public EmailNotifierException(string message)
                : base(message)
            { }

            public EmailNotifierException(string message, Exception innerException)
                : base(message, innerException)
            { }
    }
}