﻿namespace Meimo.Services.EmailNotifier.Infrastructure.Contracts
{
    public class RmqSendEmailMessage
    {
        public string From { get; set; }
        public string Cc { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
