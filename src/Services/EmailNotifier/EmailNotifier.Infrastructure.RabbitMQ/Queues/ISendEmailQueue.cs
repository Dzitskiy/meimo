﻿using Meimo.Services.EmailNotifier.Core.Infrastructure.Logging;
using Meimo.Services.EmailNotifier.Infrastructure.Contracts;

namespace Meimo.Services.EmailNotifier.Infrastructure.Queues
{
    public interface ISendEmailQueue
    {
        void AddSendEmailMessageToQueue(TracedObject<RmqSendEmailMessage> sendEmailMessageRmq);
    }
}