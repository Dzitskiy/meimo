﻿using System;
using System.Text;
using Meimo.Services.EmailNotifier.Core.Infrastructure.Logging;
using Meimo.Services.EmailNotifier.Infrastructure.Contracts;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace Meimo.Services.EmailNotifier.Infrastructure.Queues
{
    public class SendEmailQueue:ISendEmailQueue
    {
        private readonly string _hostname;
        private readonly string _password;
        private readonly string _queueName;
        private readonly string _username;
        private IConnection _connection;
        private ILogger<SendEmailQueue> _logger;
        private ITraceLogger _traceLogger;

        public SendEmailQueue(IOptions<RabbitMqConfiguration> rabbitMqOptions, ILogger<SendEmailQueue> logger, ITraceLogger traceLogger)
        {
            _queueName = rabbitMqOptions.Value.QueueName;
            _hostname = rabbitMqOptions.Value.Hostname;
            _username = rabbitMqOptions.Value.Username;
            _password = rabbitMqOptions.Value.Password;
            _logger = logger;
            _traceLogger = traceLogger;

            CreateConnection();
        }

        public void AddSendEmailMessageToQueue(TracedObject<RmqSendEmailMessage> sendEmailMessageRmq)
        {
            if (ConnectionExists())
            {
                using (var channel = _connection.CreateModel())
                {
                    channel.QueueDeclare(queue: _queueName, durable: false, exclusive: false, autoDelete: false, arguments: null);

                    var json = JsonConvert.SerializeObject(sendEmailMessageRmq);
                    var body = Encoding.UTF8.GetBytes(json);
                    _traceLogger.TraceLog(
                        string.Format("{0}.{1}", nameof(SendEmailQueue), nameof(AddSendEmailMessageToQueue)),
                        sendEmailMessageRmq);
                    channel.BasicPublish(exchange: "", routingKey: _queueName, basicProperties: null, body: body);
                }
            }
        }

        private void CreateConnection()
        {
            try
            {
                var factory = new ConnectionFactory
                {
                    HostName = _hostname,
                    UserName = _username,
                    Password = _password
                };
                _connection = factory.CreateConnection();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Could not create connection: {ex.Message} params HostName: {_hostname} QueueName: {_queueName} UserName: {_username} Password: {_password} ");
                throw;
            }
        }

        private bool ConnectionExists()
        {
            if (_connection != null)
            {
                return true;
            }

            CreateConnection();

            return _connection != null;
        }
    }
}