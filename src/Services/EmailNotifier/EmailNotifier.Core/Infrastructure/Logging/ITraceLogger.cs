﻿using System;

namespace Meimo.Services.EmailNotifier.Core.Infrastructure.Logging
{
    public interface ITraceLogger
    {
        void TraceLog<T>(string operationName, TracedObject<T> data);
        void TraceLog(Guid traceId, string operationName, string message);
    }
}