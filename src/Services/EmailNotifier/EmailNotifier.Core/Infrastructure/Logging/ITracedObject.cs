﻿using System;

namespace Meimo.Services.EmailNotifier.Core.Infrastructure.Logging
{
    public interface ITracedObject
    {
        Guid TraceId { get; }   
    }
    public class TracedObject<T>:ITracedObject
    {
        public TracedObject(T data)
        {
            Data = data;
            TraceId = Guid.NewGuid();
        }
        public T Data { get; private set; }
        public Guid TraceId { get; private set; }
    }
}