﻿using AutoMapper;
using Meimo.Services.EmailNotifier.Core.Infrastructure.Logging;
using Meimo.Services.EmailNotifier.Infrastructure.Consumers;
using Meimo.Services.EmailNotifier.Infrastructure.Contracts;
using Meimo.Services.Infrastructure.Smtp;
using Meimo.Services.Infrastructure.Smtp.Model;

namespace Meimo.Services.EmailNotifier.Daemon.Services
{
    public class SendEmailService:ISendEmailService
    {
        private readonly IEmailSender _emailSender;
        private readonly IMapper _mapper;
        
        public SendEmailService(IEmailSender emailSender, IMapper mapper)
        {
            _emailSender = emailSender;
            _mapper = mapper;
        }
        
        
        public void SendEmail(TracedObject<RmqSendEmailMessage> sendingMessageRmq)
        {
            _emailSender.SendEmailAsync(_mapper.Map<TracedObject<RmqSendEmailMessage>, TracedObject<SmtpSendEmailMessage>>(sendingMessageRmq)).Wait();
        }
    }
}