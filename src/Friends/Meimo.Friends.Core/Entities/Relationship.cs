using System;

namespace Meimo.Friends.Core
{
    public class Relationship
    {
        public Guid FirstPersonId { get; set; }
        
        public Guid SecondPersonId { get; set; }

        public RelationType? Type { get; set; }
    }
}
