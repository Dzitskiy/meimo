namespace Meimo.Friends.Core
{
    public class PaginationFilter
    {
        private const int MinPageNumber = 1;
        private const int MaxPageSize = 50;
        private const int DefaultPageSize = 10;
        
        public int PageNumber { get; }
        
        public int PageSize { get; }
        
        public PaginationFilter(int pageNumber, int pageSize)
        {
            PageNumber = pageNumber < MinPageNumber 
                ? MinPageNumber 
                : pageNumber;

            PageSize = pageSize > MaxPageSize 
                ? MaxPageSize 
                : pageSize <= 0 
                    ? DefaultPageSize 
                    : pageSize;
        }

        public PaginationFilter() : this(MinPageNumber, DefaultPageSize)
        {
            
        }
    }
}
