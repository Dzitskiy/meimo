using System;

namespace Meimo.Friends.Core
{
    public class MergeResult
    {
        public Guid Person1Id { get; set; }
        
        public bool Person1HasFirstName { get; set; }
        
        public Guid Person2Id { get; set; }
        
        public bool Person2HasFirstName { get; set; }
        
        public GraphModifyResult? ModifyResult { get; set; }

        public void SetHasFirstNameProperties(
            bool person1HasFirstName,
            bool person2HasFirstName)
        {
            Person1HasFirstName = person1HasFirstName;
            Person2HasFirstName = person2HasFirstName;
        }
    }
}
