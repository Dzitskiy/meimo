namespace Meimo.Friends.Core
{
    public class GraphModifyResult
    {
        public int NodesCreated { get; set; }

        public int NodesDeleted { get; set; }
        
        public int RelationshipsCreated { get; set; }
        
        public int RelationshipsDeleted { get; set; }
    }
}
