namespace Meimo.Friends.Infrastructure
{
    public class FriendsDbSettings : IFriendsDbSettings
    {
        public string? ConnectionString { get; set; }
        public string? User { get; set; }
        public string? Password { get; set; }
        public string? DatabaseName { get; set; }
    }
}
