using System.Text.Json;
using Confluent.Kafka;
using Meimo.Friends.Core;
using Meimo.Friends.GraphQL;
using Meimo.Friends.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Neo4j.Driver;

namespace Meimo.Friends.ProfileReplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<FriendsDbSettings>(
                        hostContext.Configuration.GetSection(nameof(FriendsDbSettings)));

                    services.AddSingleton<IFriendsDbSettings>(sp =>
                        sp.GetRequiredService<IOptions<FriendsDbSettings>>().Value);

                    services.AddSingleton<Neo4j.Driver.ILogger>(sp =>
                        new DriverLogger(sp.GetRequiredService<ILogger<DriverLogger>>()));
            
                    services.AddSingleton<IDriver>(
                        sp => GraphDatabase.Driver(
                            sp.GetRequiredService<IFriendsDbSettings>().ConnectionString, 
                            AuthTokens.Basic(sp.GetRequiredService<IFriendsDbSettings>().User, 
                                sp.GetRequiredService<IFriendsDbSettings>().Password), 
                            builder => builder.WithLogger(sp.GetRequiredService<Neo4j.Driver.ILogger>())));
                    
                    services.AddSingleton<IPersonRepository, PersonRepository>();

                    services.AddHostedService<Worker>();
                });
    }
}