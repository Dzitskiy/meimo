namespace Meimo.Friends.GraphQL
{
    public static class Constants
    {
        public const string QueryTypeName = "Query";
        public const string MutationTypeName = "Mutation";
    }
}