namespace Meimo.Friends.GraphQL
{
    public record UserError(string Message, string Code);
}
