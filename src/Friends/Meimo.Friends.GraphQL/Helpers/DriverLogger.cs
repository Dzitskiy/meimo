using System;
using Microsoft.Extensions.Logging;
using ILogger = Neo4j.Driver.ILogger;

namespace Meimo.Friends.GraphQL
{
    public class DriverLogger : ILogger
    {
        private readonly ILogger<DriverLogger> _logger;

        public DriverLogger(ILogger<DriverLogger> logger)
        {
            _logger = logger;
        }

        public void Error(Exception cause, string message, params object[] args)
        {
            _logger.LogError(default(EventId), cause, message, args);
        }

        public void Warn(Exception cause, string message, params object[] args)
        {
            _logger.LogWarning(default(EventId), cause, message, args);
        }

        public void Info(string message, params object[] args)
        {
            _logger.LogInformation(default(EventId), message, args);
        }

        public void Debug(string message, params object[] args)
        {
            _logger.LogDebug(default(EventId), message, args);
        }

        public void Trace(string message, params object[] args)
        {
            _logger.LogTrace(default(EventId), message, args);
        }

        public bool IsTraceEnabled()
        {
            return false;
        }

        public bool IsDebugEnabled()
        {
            return false;
        }
    }
}
