using System;

namespace Meimo.Friends.Kafka.Contracts
{
    public class FriendsEventMessage
    {
        /// <summary>
        /// Идентификатор инициатора события, пользователя создавшего
        /// запрос в друзья или подтвердившего добавление в друзья
        /// </summary>
        public Guid PersonId { get; set; }

        public FriendsEventMessageType MessageType { get; set; }

        public string? MessageTemplate { get; set; }
    }
}