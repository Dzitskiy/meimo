namespace Meimo.Friends.Kafka.Contracts
{
    public enum FriendsEventMessageType
    {
        Request = 1,
        Accept = 2
    }
}