﻿using System;
using System.Threading.Tasks;

namespace Meimo.Person.Account.Core.Interfaces
{
    public interface IActivationTokenService
    {
        Task<bool> CreateTokenAsync(Guid personId);

        Task<bool> MakeNewTokenAsync(Guid personId);

        Task<Guid> GetPersonIdByTokenAsync(string token);

        Task<string> GetTokenByIdAsync(Guid personId);
    }
}
