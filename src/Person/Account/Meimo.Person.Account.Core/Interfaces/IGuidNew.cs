﻿using System;

namespace Meimo.Person.Account.Core.Interfaces
{
    public interface IGuidNew
    {
        Guid Value { get; }
    }
}
