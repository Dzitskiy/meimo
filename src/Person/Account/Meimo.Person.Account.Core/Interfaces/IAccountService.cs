﻿using System;
using System.Threading.Tasks;

using Meimo.Person.Account.Core.Dtos;
using Meimo.Person.Account.Core.Entities;

namespace Meimo.Person.Account.Core.Interfaces
{
    public interface IAccountService
    {
        Task<PersonAccount> GetAccountByIdAsync(Guid id);

        Task<bool> HasPhoneNumberAsync(string phoneNumber);

        Task<bool> HasEmailAsync(string email);

        Task<PersonAccount> CreateAccountAsync(PersonAccount personAccount);

        Task<PersonAccount> GetAccountByLoginAndPasswordAsync(string login, string password);

        Task<LoginDto> GetLoginByIdAsync(Guid id);

        Task<bool> UpdatePhoneAsync(Guid id, string phone);

        Task<bool> UpdateEmailAsync(Guid id, string email);

        Task<bool> UpdatePasswordAsync(Guid id, string password, DateTimeOffset passwordDate);

        Task<bool> ActivateAccountAsync(Guid id);
    }
}
