﻿using System.ComponentModel.DataAnnotations;

namespace Meimo.Person.Account.Core.Entities
{
    public class Country
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string InvariantName { get; set; }

        [Required]
        public int SortOrder { get; set; }
    }
}
