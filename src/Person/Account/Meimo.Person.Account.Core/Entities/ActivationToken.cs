﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Meimo.Person.Account.Core.Entities
{
    public class ActivationToken
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Token { get; set; }

        public ActivationToken(Guid id, string token)
        {
            Id = id;
            Token = token;
        }
    }
}
