﻿using System;

using Meimo.Person.Account.Core.Entities;

namespace Meimo.Person.Account.Core.Dtos
{
    public class ProfileMiddleInfoDto
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Avatar { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public bool Locked { get; set; }

        public bool Deleted { get; set; }

        public ProfileMiddleInfoDto(PersonAccount personAccount)
        {
            Id = personAccount.Id;
            FirstName = personAccount.FirstName;
            LastName = personAccount.LastName;
            Avatar = personAccount.Avatar;
            City = personAccount.City;
            Country = personAccount.Country.Name;
            Locked = personAccount.Locked;
            Deleted = personAccount.Deleted;
        }

        public ProfileMiddleInfoDto(PersonAccount personAccount, string countryName)
        {
            Id = personAccount.Id;
            FirstName = personAccount.FirstName;
            LastName = personAccount.LastName;
            Avatar = personAccount.Avatar;
            City = personAccount.City;
            Country = countryName;
            Locked = personAccount.Locked;
            Deleted = personAccount.Deleted;
        }
    }
}
