﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Meimo.Person.Account.PostgreSQL.EF.DbInit.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "person");

            migrationBuilder.CreateTable(
                name: "ActivationTokens",
                schema: "person",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Token = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivationTokens", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                schema: "person",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: false),
                    InvariantName = table.Column<string>(nullable: false),
                    SortOrder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PersonAccounts",
                schema: "person",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Phone = table.Column<string>(maxLength: 30, nullable: true),
                    Email = table.Column<string>(maxLength: 100, nullable: true),
                    Password = table.Column<string>(maxLength: 30, nullable: false),
                    PasswordDate = table.Column<DateTimeOffset>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 100, nullable: false),
                    LastName = table.Column<string>(maxLength: 200, nullable: true),
                    Gender = table.Column<short>(nullable: false),
                    ShowGender = table.Column<bool>(nullable: false),
                    Birthday = table.Column<DateTimeOffset>(nullable: true),
                    ShowBirthday = table.Column<bool>(nullable: false),
                    CountryId = table.Column<int>(nullable: false),
                    SysLanguage = table.Column<short>(nullable: false),
                    City = table.Column<string>(maxLength: 100, nullable: true),
                    Avatar = table.Column<string>(nullable: true),
                    Picture = table.Column<string>(nullable: true),
                    SysRole = table.Column<int>(nullable: true),
                    Activated = table.Column<bool>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonAccounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PersonAccounts_Countries_CountryId",
                        column: x => x.CountryId,
                        principalSchema: "person",
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActivationTokens_Token",
                schema: "person",
                table: "ActivationTokens",
                column: "Token",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Countries_InvariantName",
                schema: "person",
                table: "Countries",
                column: "InvariantName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Countries_Name",
                schema: "person",
                table: "Countries",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PersonAccounts_CountryId",
                schema: "person",
                table: "PersonAccounts",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonAccounts_Email",
                schema: "person",
                table: "PersonAccounts",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PersonAccounts_Phone",
                schema: "person",
                table: "PersonAccounts",
                column: "Phone",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActivationTokens",
                schema: "person");

            migrationBuilder.DropTable(
                name: "PersonAccounts",
                schema: "person");

            migrationBuilder.DropTable(
                name: "Countries",
                schema: "person");
        }
    }
}
