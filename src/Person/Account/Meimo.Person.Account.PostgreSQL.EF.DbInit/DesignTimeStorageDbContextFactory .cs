﻿using Meimo.Person.Account.Infrastructure.Data;

using Microsoft.EntityFrameworkCore.Design;

namespace Meimo.Person.Account.PostgreSQL.EF.DbInit
{
    class DesignTimeStorageDbContextFactory : IDesignTimeDbContextFactory<StoreContext>
    {
        public StoreContext CreateDbContext(string[] args)
        {
            return DbInitContextFactory<StoreContext>.CreateDbContext();
        }
    }
}
