﻿using System;
using System.Threading.Tasks;

using Confluent.Kafka;

using Meimo.Person.Account.Core.Dtos;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Meimo.Person.Account.Infrastructure.Kafka
{
    public class KafkaProducer : IDisposable
    {
        public const string ProfileReplicationKey = "ProfileMiddleInfo";

        private readonly IProducer<string, ProfileMiddleInfoDto> _producer;
        private readonly string _topic;
        private readonly ILogger<KafkaProducer> _logger;

        public KafkaProducer(IConfiguration configuration, ILogger<KafkaProducer> logger)
        {
            var producerConfig = new ProducerConfig();
            configuration.GetSection("Kafka:ProducerSettings").Bind(producerConfig);
            producerConfig.EnableIdempotence = true;
            producerConfig.Acks = Acks.All;
            producerConfig.CompressionType = CompressionType.Gzip;
            producerConfig.CompressionLevel = 9;
            var topic = configuration.GetValue<string>("Kafka:ReplicationProfileTopic");

            _producer = new ProducerBuilder<string, ProfileMiddleInfoDto>(producerConfig)
                .SetValueSerializer(new ProfileMiddleInfoSerializer())
                .Build();
            _topic = topic;
            _logger = logger;
        }

        public async Task ProduceAsync(ProfileMiddleInfoDto profileMiddleInfoDto)
        {
            try
            {
                var deliveryResult = await _producer.ProduceAsync(_topic, new Message<string, ProfileMiddleInfoDto>
                {
                    Key = ProfileReplicationKey,
                    Value = profileMiddleInfoDto
                });

                _logger.LogInformation(
                    $"Delivered {deliveryResult.Value}, id = {deliveryResult.Value.Id} \n"+
                    $"        to topic {deliveryResult.Topic}\n" +
                    $"          partition {deliveryResult.Partition}\n" +
                    $"          offset {deliveryResult.Offset}\n" +
                    $"        key {deliveryResult.Key}\n");
            }
            catch (ProduceException<string, ProfileMiddleInfoDto> e)
            {
                _logger.LogWarning(e, $"Delivery failed: {e.Error.Reason}");
            }
        }

        public void Dispose()
        {
            _producer.Flush();
            _producer.Dispose();
        }
    }
}
