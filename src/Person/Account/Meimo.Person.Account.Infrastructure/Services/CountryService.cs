﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Meimo.Person.Account.Core.Entities;
using Meimo.Person.Account.Core.Interfaces;
using Meimo.Person.Account.Infrastructure.Data;

using Microsoft.EntityFrameworkCore;

namespace Meimo.Person.Account.Infrastructure.Services
{
    public class CountryService : ICountryService
    {
        private readonly StoreContext _context;

        public CountryService(StoreContext context)
        {
            _context = context;
        }

        public async Task<List<Country>> GetCountriesAsync()
        {
            var countries = await _context.Countries
                .OrderBy(c => c.SortOrder)
                .ThenBy(c => c.Name)
                .ToListAsync();

            return countries;
        }

        public async Task<Country> GetCountryByIdAsync(int id)
        {
            return await _context.Countries.FindAsync(id);
        }

        public async Task<bool> HasCountryAsync(int id)
        {
            return await GetCountryByIdAsync(id) != null;
        }
    }
}
