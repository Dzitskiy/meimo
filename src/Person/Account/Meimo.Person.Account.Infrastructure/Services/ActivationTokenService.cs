﻿
using System;
using System.Linq;
using System.Threading.Tasks;

using Meimo.Person.Account.Core.Entities;
using Meimo.Person.Account.Core.Helpers;
using Meimo.Person.Account.Core.Interfaces;
using Meimo.Person.Account.Infrastructure.Data;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;

namespace Meimo.Person.Account.Infrastructure.Services
{
    public class ActivationTokenService : IActivationTokenService
    {
        private readonly StoreContext _context;
        private readonly ILoggerFactory _loggerFactory;

        public ActivationTokenService(StoreContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _loggerFactory = loggerFactory;
        }

        public async Task<bool> CreateTokenAsync(Guid personId)
        {
            var token = ActivationTokenHelper.NewToken();
            var activationToken = new ActivationToken(personId, token);
            EntityEntry entityEntry = _context.ActivationTokens.Add(activationToken);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                await entityEntry.ReloadAsync();
                var logger = _loggerFactory.CreateLogger<ActivationTokenService>();
                logger.LogWarning(ex, "An error occured during create activation token");
                return false;
            }

            return true;
        }

        public async Task<bool> MakeNewTokenAsync(Guid personId)
        {
            var token = ActivationTokenHelper.NewToken();

            var activationToken = await _context.ActivationTokens.FindAsync(personId);

            EntityEntry entityEntry = null;

            if (activationToken != null)
            {
                activationToken.Token = token;
            }
            else
            {
                entityEntry = _context.ActivationTokens
                    .Add(new ActivationToken(personId, token));
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                if (entityEntry != null)
                {
                    await entityEntry.ReloadAsync();
                }

                var logger = _loggerFactory.CreateLogger<ActivationTokenService>();
                logger.LogWarning(ex, "An error occured during make new activation token");
                return false;
            }

            return true;
        }

        public async Task<Guid> GetPersonIdByTokenAsync(string token)
        {
            return await _context.ActivationTokens
                .Where(t => t.Token == token)
                .Select(t => t.Id)
                .FirstOrDefaultAsync();
        }

        public async Task<string> GetTokenByIdAsync(Guid personId)
        {
            return (await _context.ActivationTokens.FindAsync(personId))?.Token;
        }
    }
}
