﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Meimo.Person.Account.Core.Dtos;
using Meimo.Person.Account.Core.Helpers;
using Meimo.Person.Account.Core.Interfaces;
using Meimo.Person.Account.Infrastructure.Data;
using Meimo.Person.Account.Infrastructure.Kafka;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Meimo.Person.Account.Infrastructure.Services
{
    public class ProfileService : IProfileService
    {
        private readonly StoreContext _context;
        private readonly ILogger<ProfileService> _logger;
        private readonly KafkaProducer _kafkaProducer;

        public const int DefaultPageSize = 20;

        public ProfileService(StoreContext context, ILogger<ProfileService> logger, KafkaProducer kafkaProducer)
        {
            _context = context;
            _logger = logger;
            _kafkaProducer = kafkaProducer;
        }

        public async Task<PaginatedProfilesDto> GetPaginatedProfilesAsync(
            Guid pageToken, int pageSize, string firstNameStartWith)
        {
            if (pageSize < 1) pageSize = DefaultPageSize;

            firstNameStartWith = firstNameStartWith?.TrimStart(new char[] { '_', '%' , '[' , ']' });

            bool hasStartWith = !string.IsNullOrWhiteSpace(firstNameStartWith);
            bool hasPageStart = false;
            string firstName = null;

            if (pageToken != Guid.Empty)
            {
                firstName = await _context.PersonAccounts
                    .Where(p => p.Id == pageToken)
                    .Select(p => p.FirstName)
                    .FirstOrDefaultAsync();

                if (string.IsNullOrEmpty(firstName))
                {
                    throw new KeyNotFoundException("Person profile with specified page token not found");
                }

                hasPageStart = true;
            }

            var middleInfoDtos = await _context.PersonAccounts
                .Where(p =>
                    (!hasStartWith || EF.Functions.Like(p.FirstName, firstNameStartWith + "%"))
                 && (!hasPageStart || ( (p.FirstName == firstName && p.Id.CompareTo(pageToken) >= 0)
                                        || p.FirstName.CompareTo(firstName) > 0 )))
                .Include(p => p.Country)
                .OrderBy(p => p.FirstName).ThenBy(p => p.Id)
                .Take(pageSize + 1)
                .Select(p => new ProfileMiddleInfoDto(p))
                .ToListAsync();

            Guid nextPageToken = Guid.Empty;

            if (middleInfoDtos.Count > pageSize)
            {
                nextPageToken = middleInfoDtos[middleInfoDtos.Count - 1].Id;
                middleInfoDtos.RemoveAt(middleInfoDtos.Count - 1);
            }

            var result = new PaginatedProfilesDto
            {
                Profiles = middleInfoDtos,
                NextPageToken = nextPageToken
            };

            return result;
        }

        public async Task<ProfileStatusInfoDto> GetProfileStatusAsync(Guid id)
        {
            var status = await _context.PersonAccounts
                .Where(a => a.Id == id)
                .Select(a => new ProfileStatusInfoDto(a.Activated, a.Locked, a.Deleted))
                .FirstOrDefaultAsync();

            return status;
        }

        public async Task<ProfileShortInfoDto> GetProfileShortInfoAsync(Guid id)
        {
            var result = await _context.PersonAccounts
                .Where(a => a.Id == id)
                .Select(a => new ProfileShortInfoDto
                    {
                        FirstName = a.FirstName,
                        LastName = a.LastName,
                        Avatar = a.Avatar,
                        Locked = a.Locked,
                        Deleted = a.Deleted
                    })
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<ProfileMiddleInfoDto> GetProfileMiddleInfoAsync(Guid id)
        {
            var result = await _context.PersonAccounts
                .Where(a => a.Id == id)
                .Include(a => a.Country)
                .Select(a => new ProfileMiddleInfoDto(a))
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<ProfileDto> GetProfileInfoAsync(Guid id)
        {
            var result = await _context.PersonAccounts
                .Where(a => a.Id == id)
                .Include(a => a.Country)
                .Select(a => new ProfileDto
                    {
                        FirstName = a.FirstName,
                        LastName = a.LastName,
                        Gender = a.Gender,
                        GenderName = EnumHelper.GetEnumMember(a.Gender),
                        ShowGender = a.ShowGender,
                        Birthday = a.Birthday,
                        ShowBirthday = a.ShowBirthday,
                        CountryId = a.CountryId,
                        Country = a.Country,
                        City = a.City
                    })
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task UpdateProfileAsync(ProfileUpdateDto profileUpdateDto)
        {
            if (profileUpdateDto is null) throw new ArgumentNullException();

            var profile = await _context.PersonAccounts.FindAsync(profileUpdateDto.Id);

            if (profile is null) throw new KeyNotFoundException("Person profile with specified key not found");

            profile.FirstName = profileUpdateDto.FirstName;
            profile.LastName = profileUpdateDto.LastName;
            profile.Gender = profileUpdateDto.Gender;
            profile.ShowGender = profileUpdateDto.ShowGender;
            profile.Birthday = profileUpdateDto.Birthday;
            profile.ShowBirthday = profileUpdateDto.ShowBirthday;
            profile.CountryId = profileUpdateDto.CountryId;
            profile.City = profileUpdateDto.City;

            await _context.SaveChangesAsync();

            var countryName = await GetCountryNameAsync(profile.CountryId);
            await SendToReplicateAsync(new ProfileMiddleInfoDto(profile, countryName));
        }

        public async Task<PictureInfoDto> GetPictureInfoAsync(Guid id)
        {
            var result = await _context.PersonAccounts
                .Where(a => a.Id == id)
                .Select(a => new PictureInfoDto { Avatar = a.Avatar, Picture = a.Picture })
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task UpdatePictureInfoAsync(PictureInfoUpdateDto pictureInfoUpdateDto)
        {
            if (pictureInfoUpdateDto is null) throw new ArgumentNullException();

            var profile = await _context.PersonAccounts.FindAsync(pictureInfoUpdateDto.Id);

            if (profile is null) throw new KeyNotFoundException("Person profile with specified key not found");

            profile.Avatar = pictureInfoUpdateDto.Avatar;
            profile.Picture = pictureInfoUpdateDto.Picture;

            await _context.SaveChangesAsync();

            var countryName = await GetCountryNameAsync(profile.CountryId);
            await SendToReplicateAsync(new ProfileMiddleInfoDto(profile, countryName));
        }

        public async Task<string> GetCountryNameAsync(int id)
        {
            return await _context.Countries
                .Where(c => c.Id == id)
                .Select(c => c.Name)
                .FirstOrDefaultAsync();
        }

        public async Task SendToReplicateAsync(ProfileMiddleInfoDto profileMiddleInfoDto)
        {
            await _kafkaProducer.ProduceAsync(profileMiddleInfoDto);
        }
    }
}
