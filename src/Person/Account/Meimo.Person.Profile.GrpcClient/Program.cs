﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Grpc.Net.Client;

namespace Meimo.Person.Profile.GrpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            AppContext.SetSwitch(
                "System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            // The port number(5000) must match the port of the gRPC server.
            using var channel = GrpcChannel.ForAddress("http://localhost:5002");
            var client = new Profiles.ProfilesClient(channel);

            string id = "e5da39c8-c1bf-4414-8015-291972a58fc1";
            var replyStatus = await client.GetProfileStatusAsync(request: new ProfileLookupModel
            {
                Id = id
            }, deadline: DateTime.UtcNow.AddSeconds(10));

            Console.WriteLine(
                $"Id = {id}\nStatus:\n" + 
                $"- AccessIsAllowed = {replyStatus.AccessIsAllowed}\n" +
                $"- Activated = {replyStatus.Activated}\n" +
                $"- Locked = {replyStatus.Locked}\n" +
                $"- Deleted = {replyStatus.Deleted}\n");


            var reply = await client.GetPaginatedProfilesAsync(new PaginatedProfilesRequest 
            { 
                //Parent = "",
                PageSize = 100,
                //PageToken = "",
                //FirstNameStartWith = ""
            });

            reply.Profiles.ToList()
                .ForEach(p => Console.WriteLine(
                    $"{p.Id}  {p.FirstName.PadRight(20)}  {(p.LastName??string.Empty).PadRight(20)}"+
                    $"  {(p.City??string.Empty).PadRight(20)}  {p.Country}"));

            Console.WriteLine("\n Press any key to continue ...");
            Console.ReadKey();
        }
    }
}
