﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Grpc.Core;

using Meimo.Person.Account.Core.Dtos;
using Meimo.Person.Account.Core.Interfaces;

using Microsoft.Extensions.Logging;

namespace Meimo.Person.Profile.GrpcServer
{
    public class ProfilesService : Profiles.ProfilesBase
    {
        private readonly ILogger<ProfilesService> _logger;
        private readonly IProfileService _profileService;

        public ProfilesService(ILogger<ProfilesService> logger, IProfileService profileService)
        {
            _logger = logger;
            _profileService = profileService;
        }

        private Guid ParseProfileLookupModel(ProfileLookupModel lookupModel)
        {
            if (!Guid.TryParse(lookupModel.Id, out Guid id))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Profile Id must be a GUID"));
            }

            return id;
        }

        private async Task<ProfileDto> GetProfileInfoAsync(ProfileLookupModel request)
        {
            Guid id = ParseProfileLookupModel(request);

            var profileDto = await _profileService.GetProfileInfoAsync(id);

            if (profileDto == null) throw new RpcException(new Status(StatusCode.NotFound, "Profile Not Found"));

            return profileDto;
        }

        public override async Task<PaginatedProfilesReply> GetPaginatedProfiles(PaginatedProfilesRequest request, ServerCallContext context)
        {
            bool hasParent = !string.IsNullOrWhiteSpace(request.Parent);
            bool hasPageToken = !string.IsNullOrWhiteSpace(request.PageToken);

            if (hasParent && !hasPageToken)
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument,
                    $"No page token specified"));
            }

            if (hasParent && !Guid.TryParse(request.Parent, out _))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument,
                    $"{nameof(request.Parent)} must be a GUID"));
            }

            Guid pageToken = Guid.Empty;

            if (hasPageToken && !Guid.TryParse(request.PageToken, out pageToken))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument,
                    $"{request.PageToken} must be a GUID"));
            }

            PaginatedProfilesDto paginatedProfilesDto;

            try
            {
                paginatedProfilesDto = await _profileService.GetPaginatedProfilesAsync(
                    pageToken, request.PageSize, request.FirstNameStartWith);
            }
            catch (KeyNotFoundException e)
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, e.Message));
            }

            var result = new PaginatedProfilesReply();

            paginatedProfilesDto.Profiles
                .ForEach(p => result.Profiles.Add(ProfileMiddleReplyModel.FromRepositoryModel(p)));

            result.NextPageToken = paginatedProfilesDto.NextPageToken != Guid.Empty
                ? paginatedProfilesDto.NextPageToken.ToString()
                : string.Empty;

            return result;
        }

        public override async Task<ProfileStatusReplyModel> GetProfileStatus(ProfileLookupModel request, ServerCallContext context)
        {
            Guid id = ParseProfileLookupModel(request);

            var profileStatus = await _profileService.GetProfileStatusAsync(id);

            if (profileStatus == null) throw new RpcException(new Status(StatusCode.NotFound, "Profile Not Found"));

            return ProfileStatusReplyModel.FromRepositoryModel(profileStatus);
        }

        public override async Task<ProfileShortReplyModel> GetProfileShortInfo(ProfileLookupModel request, ServerCallContext context)
        {
            Guid id = ParseProfileLookupModel(request);

            var profileDto = await _profileService.GetProfileShortInfoAsync(id);

            if (profileDto == null) throw new RpcException(new Status(StatusCode.NotFound, "Profile Not Found"));

            return ProfileShortReplyModel.FromRepositoryModel(profileDto);
        }

        public override async Task<ProfileMiddleReplyModel> GetProfileMiddleInfo(ProfileLookupModel request, ServerCallContext context)
        {
            Guid id = ParseProfileLookupModel(request);

            var profileDto = await _profileService.GetProfileMiddleInfoAsync(id);

            if (profileDto == null) throw new RpcException(new Status(StatusCode.NotFound, "Profile Not Found"));

            return ProfileMiddleReplyModel.FromRepositoryModel(profileDto);
        }

        public override async Task<ProfileRestrictedReplyModel> GetProfileRestrictedInfo(ProfileLookupModel request, ServerCallContext context)
        {
            return ProfileRestrictedReplyModel.FromRepositoryModel(await GetProfileInfoAsync(request));
        }

        public override async Task<ProfileReplyModel> GetProfileInfo(ProfileLookupModel request, ServerCallContext context)
        {
            return ProfileReplyModel.FromRepositoryModel(await GetProfileInfoAsync(request));
        }

        public override async Task<ProfileChangeReplyModel> ChangeProfileInfo(ProfileChangeRequestModel request, ServerCallContext context)
        {
            ProfileChangeRequestModel.Validate(request);

            var profileUpdateDto = ProfileChangeRequestModel.ToRepositoryModel(request);

            try
            {
                await _profileService.UpdateProfileAsync(profileUpdateDto);
            }
            catch (KeyNotFoundException)
            {
                throw new RpcException(new Status(StatusCode.NotFound, "Profile Not Found"));
            }
            catch (Exception ex)
            {
                Exception innerException = ex.InnerException;
                string message = ex.InnerException?.Message ?? ex.Message;

                if (!(innerException is null) &&
                    innerException.GetType() == typeof(Npgsql.PostgresException))
                {
                    var pgsqlException = (Npgsql.PostgresException)innerException;

                    if (pgsqlException.SqlState == "23503")
                    {
                        if (pgsqlException.ConstraintName == "FK_PersonAccounts_Countries_CountryId")
                        {
                            throw new RpcException(new Status(StatusCode.InvalidArgument,
                                "Country with specified countryId not found"));
                        }

                        throw new RpcException(new Status(StatusCode.InvalidArgument,
                            "Update violates a foreign key constraint"));
                    }
                }

                throw new RpcException(new Status(StatusCode.Unknown, message));
            }

            return new ProfileChangeReplyModel { CompletedSuccessfully = true };
        }

        public override async Task<ProfilePictureReplyModel> GetProfilePictureInfo(ProfileLookupModel request, ServerCallContext context)
        {
            Guid id = ParseProfileLookupModel(request);

            var pictureDto = await _profileService.GetPictureInfoAsync(id);

            if (pictureDto == null) throw new RpcException(new Status(StatusCode.NotFound, "Profile Not Found"));

            return ProfilePictureReplyModel.FromRepositoryModel(pictureDto);
        }

        public override async Task<ProfileChangeReplyModel> ChangeProfilePictureInfo(ProfilePictureChangeRequestModel request, ServerCallContext context)
        {
            ProfilePictureChangeRequestModel.Validate(request);

            var pictureInfoUpdateDto = ProfilePictureChangeRequestModel.ToRepositoryModel(request);

            try
            {
                await _profileService.UpdatePictureInfoAsync(pictureInfoUpdateDto);
            }
            catch (KeyNotFoundException)
            {
                throw new RpcException(new Status(StatusCode.NotFound, "Profile Not Found"));
            }
            catch (Exception ex)
            {
                throw new RpcException(new Status(StatusCode.Unknown,
                    ex.InnerException?.Message ?? ex.Message));
            }

            return new ProfileChangeReplyModel { CompletedSuccessfully = true };
        }
    }
}
