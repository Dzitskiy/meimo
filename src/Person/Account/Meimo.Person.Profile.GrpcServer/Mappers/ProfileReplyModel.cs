﻿using Google.Protobuf.WellKnownTypes;

using Meimo.Person.Account.Core.Dtos;

namespace Meimo.Person.Profile.GrpcServer
{
    public sealed partial class ProfileReplyModel
    {
        public static ProfileReplyModel FromRepositoryModel(ProfileDto source)
        {
            if (source is null) return null;

            return new ProfileReplyModel
            {
                FirstName = source.FirstName,
                LastName = source.LastName,
                Gender = (int)source.Gender,
                GenderName = source.GenderName,
                ShowGender = source.ShowGender,
                Birthday = source.Birthday.HasValue
                    ? Timestamp.FromDateTimeOffset(source.Birthday.Value)
                    : null,
                ShowBirthday = source.ShowBirthday,
                CountryId = source.CountryId,
                Country = source.Country.Name,
                City = source.City,
            };
        }
    }
}
