﻿using Google.Protobuf.WellKnownTypes;

using Meimo.Person.Account.Core.Dtos;

namespace Meimo.Person.Profile.GrpcServer
{
    public sealed partial class ProfileRestrictedReplyModel
    {
        public static ProfileRestrictedReplyModel FromRepositoryModel(ProfileDto source)
        {
            if (source is null) return null;

            return new ProfileRestrictedReplyModel
            {
                FirstName = source.FirstName,
                LastName = source.LastName,
                GenderName = source.ShowGender ? source.GenderName : null,
                Birthday = source.Birthday.HasValue && source.ShowBirthday
                    ? Timestamp.FromDateTimeOffset(source.Birthday.Value)
                    : null,
                Country = source.Country.Name,
                City = source.City,
            };
        }
    }
}
