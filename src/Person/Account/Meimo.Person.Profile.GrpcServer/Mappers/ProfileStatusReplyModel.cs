﻿
using Meimo.Person.Account.Core.Dtos;

namespace Meimo.Person.Profile.GrpcServer
{
    public sealed partial class ProfileStatusReplyModel
    {
        public static ProfileStatusReplyModel FromRepositoryModel(ProfileStatusInfoDto source)
        {
            if (source is null) return null;

            return new ProfileStatusReplyModel
            {
                AccessIsAllowed = source.AccessIsAllowed,
                Activated = source.Activated,
                Locked = source.Locked,
                Deleted = source.Deleted
            };
        }
    }
}
