﻿
using Meimo.Person.Account.Core.Dtos;

namespace Meimo.Person.Profile.GrpcServer
{
    public sealed partial class ProfilePictureReplyModel
    {
        public static ProfilePictureReplyModel FromRepositoryModel(PictureInfoDto source)
        {
            if (source is null) return null;

            return new ProfilePictureReplyModel
            {
                Avatar = source.Avatar,
                Picture = source.Picture,
            };
        }
    }
}
