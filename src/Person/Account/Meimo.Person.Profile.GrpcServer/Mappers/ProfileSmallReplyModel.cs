﻿
using Meimo.Person.Account.Core.Dtos;

namespace Meimo.Person.Profile.GrpcServer
{
    public sealed partial class ProfileShortReplyModel
    {
        public static ProfileShortReplyModel FromRepositoryModel(ProfileShortInfoDto source)
        {
            if (source is null) return null;

            return new ProfileShortReplyModel
            {
                FirstName = source.FirstName,
                LastName = source.LastName,
                Avatar = source.Avatar,
                Locked = source.Locked,
                Deleted = source.Deleted
            };
        }
    }
}
