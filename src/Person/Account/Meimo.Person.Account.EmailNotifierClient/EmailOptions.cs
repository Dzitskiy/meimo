﻿namespace Meimo.Person.Account.EmailNotifierClient
{
    public class EmailOptions
    {
        public const string EmailConfig = "EmailConfig";

        public string FromAddress { get; set; }
        public string TemplateId { get; set; }
    }
}
