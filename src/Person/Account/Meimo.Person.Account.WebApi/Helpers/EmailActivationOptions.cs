﻿namespace Meimo.Person.Account.WebApi.Helpers
{
    public class EmailActivationOptions
    {
        public const string EmailActivationConfig = "EmailActivationConfig";

        public string ActivationUrl { get; set; }
    }
}
