﻿using Meimo.Person.Account.Core.Helpers;

namespace Meimo.Person.Account.WebApi.Errors
{
    public class AuthorizationError
    {
        public int Code { get; set; }

        public string Name { get; set; }

        public string Message { get; set; }

        public AuthorizationError(AuthorizationErrorCode authorizationErrorCode)
        {
            Code = (int)authorizationErrorCode;
            Name = authorizationErrorCode.ToString();
            Message = authorizationErrorCode.GetEnumMember(); 
        }
    }
}
