﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Meimo.Person.Account.Core.Interfaces;
using Meimo.Person.Account.EmailNotifierClient;
using Meimo.Person.Account.WebApi.Errors;
using Meimo.Person.Account.WebApi.Helpers;
using Meimo.Person.Account.WebApi.Models;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Meimo.Person.Account.WebApi.Controllers
{
    public class AccountsController : BaseApiController
    {
        private readonly ILogger<AccountsController> _logger;
        private readonly IDateTimeOffsetNow _dateTimeOffsetNow;
        private readonly IGuidNew _guidNew;
        private readonly IAccountService _accountService;
        private readonly ICountryService _countryService;
        private readonly IActivationTokenService _activationTokenService;
        private readonly IEmailSender _emailSender;
        private readonly EmailActivationOptions _emailActivationOptions;

        public AccountsController(
            ILogger<AccountsController> logger,
            IDateTimeOffsetNow dateTimeOffsetNow,
            IGuidNew guidNew,
            IAccountService accountService,
            ICountryService countryService,
            IActivationTokenService activationTokenService,
            IEmailSender emailSender,
            EmailActivationOptions emailActivationOptions)
        {
            _logger = logger;
            _dateTimeOffsetNow = dateTimeOffsetNow;
            _guidNew = guidNew;
            _accountService = accountService;
            _countryService = countryService;
            _activationTokenService = activationTokenService;
            _emailSender = emailSender;
            _emailActivationOptions = emailActivationOptions;
        }

        /// <summary>
        /// Зарегистрировать в системе нового пользователя
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("signup")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponseExtended), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status409Conflict)]
        public async Task<ActionResult<SignUpResponse>> SignUpAsync(
            [FromBody] SignUpRequest request)
        {
            if (request == null)
            {
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest,
                    $"{nameof(SignUpRequest)} object is null"));
            }

            if (!await _countryService.HasCountryAsync(request.CountryId))
            {
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest,
                    "Страна с указанным идентификатором отсутствует в базе данных"));
            }

            if (!string.IsNullOrWhiteSpace(request.PhoneNumber)
                && await _accountService.HasPhoneNumberAsync(request.PhoneNumber))
            {
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest,
                    "Аккаунт с указанным номером телефона уже существует"));
            }

            if (!string.IsNullOrWhiteSpace(request.Email)
                && await _accountService.HasEmailAsync(request.Email))
            {
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest,
                    "Аккаунт с указанным адресом электронной почты уже существует"));
            }

            var personAccount = request.MapToPersonAccount(_dateTimeOffsetNow, _guidNew);

            var addedPersonAccount = await _accountService.CreateAccountAsync(personAccount);

            if (addedPersonAccount == null)
            {
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict,
                    "При создании аккаунта произошла непредвиденная ошибка, повторите попытку позже"));
            }

            var signUpResponse = new SignUpResponse(addedPersonAccount.Id, addedPersonAccount.Activated);

            if (!addedPersonAccount.Activated)
            {
                signUpResponse.ActivationTokenCreated = await _activationTokenService
                    .CreateTokenAsync(addedPersonAccount.Id);

                if (signUpResponse.ActivationTokenCreated)
                {
                    signUpResponse.ActivationRequestSended = await SendActivationEmailHelper
                        .SendActivationRequestAsync(
                            _emailSender,
                            _logger,
                            _emailActivationOptions,
                            addedPersonAccount.Email,
                            await _activationTokenService.GetTokenByIdAsync(addedPersonAccount.Id));
                }
            }

            return Ok(signUpResponse);
        }

        /// <summary>
        /// Выполнить аутентификацию и авторизацию
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("signin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponseExtended), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(SignInFailedResponse), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<SignInResponse>> SignInAsync(
            [FromBody] SignInRequest request)
        {
            if (request == null)
            {
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest,
                    $"{nameof(SignInRequest)} object is null"));
            }

            var personAccount = await _accountService
                .GetAccountByLoginAndPasswordAsync(request.Login, request.Password);

            // проверка аутентификации
            if (personAccount == null)
            {
                return NotFound(new ApiResponse(StatusCodes.Status404NotFound,
                    "Неверное имя пользователя или пароль"));
            }

            // проверка авторизации
            var authorizationErrors = new List<AuthorizationError>();

            if (!personAccount.Activated)
            {
                authorizationErrors.Add(new AuthorizationError(AuthorizationErrorCode.NotActivated));
            }

            if (personAccount.Locked)
            {
                authorizationErrors.Add(new AuthorizationError(AuthorizationErrorCode.Locked));
            }

            if (personAccount.Deleted)
            {
                authorizationErrors.Add(new AuthorizationError(AuthorizationErrorCode.Deleted));
            }

            if (authorizationErrors.Count > 0)
            {
                var signInFailedResponse = new SignInFailedResponse()
                {
                    Authenticated = true,
                    Authorized = false,
                    AuthorizationErrors = authorizationErrors
                };

                return Unauthorized(signInFailedResponse);
            }

            return Ok(new SignInResponse(personAccount.Id));
        }

        /// <summary>
        /// Получить логин по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/login")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponseExtended), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<LoginResponse>> GetLoginAsync([FromRoute] Guid id)
        {
            var loginDto= await _accountService.GetLoginByIdAsync(id);

            if (loginDto == null)
            {
                return NotFound(new ApiResponse(StatusCodes.Status404NotFound));
            }

            return Ok(new LoginResponse(loginDto));
        }

        /// <summary>
        /// Изменить телефон пользователя
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPatch("{id}/phone")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ApiResponseExtended), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status409Conflict)]
        public async Task<ActionResult> UpdatePhoneAsync([FromRoute] Guid id,
            [FromBody] ChangePhoneRequest request)
        {
            if (request == null)
            {
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest,
                    $"{nameof(ChangePhoneRequest)} object is null"));
            }

            if (string.IsNullOrWhiteSpace(request.Phone))
            {
                request.Phone = null;
            }

            var personAccount = await _accountService.GetAccountByIdAsync(id);

            if (personAccount == null)
            {
                return NotFound(new ApiResponse(StatusCodes.Status404NotFound,
                    "Пользователь с указанным идентификатором не найден"));
            }

            var oldPhone = personAccount.Phone;

            if (personAccount.Password != request.Password)
            {
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest,
                    "Неверный пароль"));
            }

            if (!string.IsNullOrWhiteSpace(request.Phone)
                && await _accountService.HasPhoneNumberAsync(request.Phone))
            {
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest,
                    "Аккаунт с указанным номером телефона уже существует"));
            }

            var isUpdated = await _accountService.UpdatePhoneAsync(id, request.Phone);

            if (!isUpdated)
            {
                var loginDto = await _accountService.GetLoginByIdAsync(id);

                _logger.LogWarning($"Неизвестная ошибка при изменении телефона пользователя {id}"
                    + $" старый телефон - {oldPhone} новый телефон - {request.Phone}"
                    + $" в БД после выполнения операции - {loginDto.Phone}");

                return Conflict(new ApiResponse(StatusCodes.Status409Conflict));
            }

            return NoContent();
        }

        /// <summary>
        /// Изменить адрес электронной почты пользователя
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPatch("{id}/email")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ApiResponseExtended), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status409Conflict)]
        public async Task<ActionResult> UpdateEmailAsync([FromRoute] Guid id,
            [FromBody] ChangeEmailRequest request)
        {
            if (request == null)
            {
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest,
                    $"{nameof(ChangeEmailRequest)} object is null"));
            }

            if (string.IsNullOrWhiteSpace(request.Email))
            {
                request.Email = null;
            }

            var personAccount = await _accountService.GetAccountByIdAsync(id);

            if (personAccount == null)
            {
                return NotFound(new ApiResponse(StatusCodes.Status404NotFound,
                    "Пользователь с указанным идентификатором не найден"));
            }

            var oldEmail = personAccount.Email;

            if (personAccount.Password != request.Password)
            {
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest,
                    "Неверный пароль"));
            }

            if (!string.IsNullOrWhiteSpace(request.Email)
                && await _accountService.HasEmailAsync(request.Email))
            {
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest,
                    "Аккаунт с указанным адресом электронной почты уже существует"));
            }

            bool isUpdated = await _accountService.UpdateEmailAsync(id, request.Email);

            if (!isUpdated)
            {
                var loginDto = await _accountService.GetLoginByIdAsync(id);

                _logger.LogWarning($"Неизвестная ошибка при изменении email пользователя {id}"
                    + $" старый email - {oldEmail} новый email - {request.Email}"
                    + $" в БД после выполнения операции - {loginDto.Email}");

                return Conflict(new ApiResponse(StatusCodes.Status409Conflict));
            }

            return NoContent();
        }

        /// <summary>
        /// Изменить пароль пользователя
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPatch("{id}/password")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ApiResponseExtended), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status409Conflict)]
        public async Task<ActionResult> ChangePasswordAsync([FromRoute] Guid id,
            [FromBody] ChangePasswordRequest request)
        {
            if (request == null)
            {
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest,
                    $"{nameof(ChangePasswordRequest)} object is null"));
            }

            var personAccount = await _accountService.GetAccountByIdAsync(id);

            if (personAccount == null)
            {
                return NotFound(new ApiResponse(StatusCodes.Status404NotFound,
                    "Пользователь с указанным идентификатором не найден"));
            }

            if (personAccount.Password != request.OldPassword)
            {
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest,
                    "Неверный действующий пароль"));
            }

            bool isUpdated = await _accountService.UpdatePasswordAsync(
                id, request.Password, _dateTimeOffsetNow.Value);

            if (!isUpdated)
            {
                _logger.LogWarning($"Неизвестная ошибка при изменении пароля пользователя {id}");

                return Conflict(new ApiResponse(StatusCodes.Status409Conflict));
            }

            return NoContent();
        }

        /// <summary>
        /// Активировать аккаунт пользователя
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("activate/{token}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status409Conflict)]
        public async Task<ActionResult> ActivateAccountAsync([FromRoute] string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest,
                    "Токен активации не задан"));
            }

            Guid personId = await _activationTokenService.GetPersonIdByTokenAsync(token);

            if (personId == Guid.Empty)
            {
                return NotFound(new ApiResponse(StatusCodes.Status404NotFound,
                    "Токен активации не найден в системе"));
            }

            bool isActivated = await _accountService.ActivateAccountAsync(personId);

            if (!isActivated)
            {
                _logger.LogWarning($"Неизвестная ошибка при активации аккаунта пользователя {personId}");

                return Conflict(new ApiResponse(StatusCodes.Status409Conflict,
                    "При активации аккаунта произошла непредвиденная ошибка, повторите попытку позже"));
            }

            return NoContent();
        }
    }
}
