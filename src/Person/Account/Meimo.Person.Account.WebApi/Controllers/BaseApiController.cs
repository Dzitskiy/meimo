﻿
using Microsoft.AspNetCore.Mvc;

namespace Meimo.Person.Account.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BaseApiController : ControllerBase
    {

    }
}
