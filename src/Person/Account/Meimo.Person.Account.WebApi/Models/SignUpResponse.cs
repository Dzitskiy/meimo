﻿using System;

namespace Meimo.Person.Account.WebApi.Models
{
    public class SignUpResponse
    {
        public Guid PersonId { get; set; }

        public bool Activated { get; set; }

        public bool ActivationTokenCreated { get; set; }

        public bool ActivationRequestSended { get; set; }

        public SignUpResponse(
            Guid personId,
            bool activated,
            bool activationTokenCreated = false,
            bool activationRequestSended = false)
        {
            PersonId = personId;
            Activated = activated;
            ActivationTokenCreated = activationTokenCreated;
            ActivationRequestSended = activationRequestSended;
        }
    }
}
