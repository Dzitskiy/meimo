﻿using System.ComponentModel.DataAnnotations;

namespace Meimo.Person.Account.WebApi.Models
{
    public class MakeNewTokenRequest
    {
        [Required]
        [MaxLength(100), MinLength(5)]
        [EmailAddress]
        public string Login { get; set; }

        [Required]
        [MaxLength(30), MinLength(5)]
        public string Password { get; set; }
    }
}
