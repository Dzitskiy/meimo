﻿using System.Collections.Generic;

using Meimo.Person.Account.WebApi.Errors;

namespace Meimo.Person.Account.WebApi.Models
{
    public class SignInFailedResponse
    {
        public bool Authenticated { get; set; }

        public bool Authorized { get; set; }

        public List<AuthorizationError> AuthorizationErrors { get; set; }
    }
}
