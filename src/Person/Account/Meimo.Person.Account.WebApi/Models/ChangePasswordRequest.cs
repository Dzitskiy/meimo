﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Meimo.Person.Account.WebApi.Helpers;

namespace Meimo.Person.Account.WebApi.Models
{
    public class ChangePasswordRequest : IValidatableObject
    {
        [Required]
        [MaxLength(30), MinLength(5)]
        public string OldPassword { get; set; }

        [Required]
        [MaxLength(30), MinLength(5)]
        public string Password { get; set; }

        [Required]
        [MaxLength(30), MinLength(5)]
        public string PasswordConfirmation { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var passwordDifferenceValidationResult = PasswordHelper.ValidatePasswordDifference(
                Password,
                OldPassword,
                nameof(Password));

            if (passwordDifferenceValidationResult != ValidationResult.Success)
            {
                yield return passwordDifferenceValidationResult;
            }

            var passwordConfirmationValidationResult = PasswordHelper.ValidatePasswordConfirmation(
                Password,
                PasswordConfirmation,
                nameof(Password),
                nameof(PasswordConfirmation));

            if (passwordConfirmationValidationResult != ValidationResult.Success)
            {
                yield return passwordConfirmationValidationResult;
            }
        }
    }
}
