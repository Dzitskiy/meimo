﻿using System.ComponentModel.DataAnnotations;

namespace Meimo.Person.Account.WebApi.Models
{
    public class ChangeEmailRequest
    {
        [Required]
        [MaxLength(100), MinLength(5)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MaxLength(30), MinLength(5)]
        public string Password { get; set; }
    }
}
