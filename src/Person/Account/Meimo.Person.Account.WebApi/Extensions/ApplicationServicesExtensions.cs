﻿using System;
using System.Linq;

using Meimo.Person.Account.Core.Helpers;
using Meimo.Person.Account.Core.Interfaces;
using Meimo.Person.Account.EmailNotifierClient;
using Meimo.Person.Account.Infrastructure.Services;
using Meimo.Person.Account.WebApi.Errors;
using Meimo.Person.Account.WebApi.Helpers;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Meimo.Person.Account.WebApi.Extensions
{
    public static class ApplicationServicesExtensions
    {
        public static IServiceCollection AddApplicationServices(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddTransient<IDateTimeOffsetNow, DateTimeOffsetNow>();
            services.AddTransient<IGuidNew, GuidNew>();

            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<IActivationTokenService, ActivationTokenService>();

            var emailOptions = new EmailOptions();
            configuration.GetSection(EmailOptions.EmailConfig).Bind(emailOptions);
            services.AddSingleton(emailOptions);

            var emailActivationOptions = new EmailActivationOptions();
            configuration.GetSection(EmailActivationOptions.EmailActivationConfig)
                .Bind(emailActivationOptions);
            services.AddSingleton(emailActivationOptions);

            services.AddHttpClient<IEmailSender, EmailSender>(c =>
            {
                c.BaseAddress = new Uri(configuration.GetConnectionString("EmailNotifier"));
                c.DefaultRequestHeaders.Add("Accept", "application/json");
                c.DefaultRequestHeaders.Add("User-Agent", "Account HttpClient");
            });

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = actionContext =>
                {
                    var errors = actionContext.ModelState
                        .Where(x => x.Value.Errors.Count > 0)
                        .Select(x => new ModelErrorKeyValues(x.Key, x.Value.Errors
                            .Select(e => e.ErrorMessage)
                            .ToArray()))
                        .ToArray();

                    var errorResponse = new ApiResponseExtended
                    {
                        Errors = errors
                    };

                    return new BadRequestObjectResult(errorResponse);
                };
            });

            return services;
        }
    }
}
