﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meimo.Person.AdditionalInfo.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}
