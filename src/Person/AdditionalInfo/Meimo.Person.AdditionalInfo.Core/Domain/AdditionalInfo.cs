﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meimo.Person.AdditionalInfo.Core.Domain
{
    public class AdditionalInfoEntity : BaseEntity
    {
        public Guid PersonId { get; set; }
        public string About { get; set; }
        public ICollection<EducationEntity> Educations { get; set; }
        public ICollection<string> Sports { get; set; }
        public ICollection<string> Musics { get; set; }
        public ICollection<string> Movies { get; set; }
        public ICollection<string> Films { get; set; }
        public ICollection<string> Games { get; set; }
    }
}
