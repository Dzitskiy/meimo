﻿using MongoDB.Bson;
using System;

namespace Meimo.Person.AdditionalInfo.WebApi.Models
{
    public class BaseItemResponse
    {
        public ObjectId Id { get; set; }
        public string Description { get; set; }
    }
}
