﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meimo.Person.AdditionalInfo.WebApi.Models
{
    public class PersonAdditionalInfoResponse
    {
        public Guid PersonId { get; set; }
        public string About { get; set; }
        public List<string> Sports { get; set; } = new List<string>();
        public List<string> Musics { get; set; } = new List<string>();
        public List<string> Movies { get; set; } = new List<string>();
        public List<string> Films { get; set; } = new List<string>();
        public List<string> Games { get; set; } = new List<string>();
        public List<EducationResponse> Educations { get; set; } = new List<EducationResponse>();
    }
    public class CreatePersonAdditionalInfoRequest
    {
        public Guid PersonId { get; set; }
        public string About { get; set; }
        public List<string> Sports { get; set; } = new List<string>();
        public List<string> Musics { get; set; } = new List<string>();
        public List<string> Movies { get; set; } = new List<string>();
        public List<string> Films { get; set; } = new List<string>();
        public List<string> Games { get; set; } = new List<string>();
        public List<EducationRequest> Educations { get; set; } = new List<EducationRequest>();
    }
}
