#!/bin/bash

# turn on bash's job control
set -m

# Start the primary process and put it in the background
/docker-entrypoint.sh neo4j &

# Run cypher scripts
/script-runner.sh

# now we bring the primary process back into the foreground
# and leave it there
fg %1