CREATE CONSTRAINT constraint_person_id IF NOT EXISTS ON (n:Person) ASSERT n.id IS UNIQUE;
CREATE CONSTRAINT constraint_person_guid IF NOT EXISTS ON (n:Person) ASSERT n.guid IS UNIQUE;