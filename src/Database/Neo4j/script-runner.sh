#!/bin/bash

export NEO4J_USERNAME=$NEO4J_USERNAME
export NEO4J_PASSWORD=$NEO4J_PASSWORD

# wait for Neo4j
wget --quiet --tries=10 --waitretry=2 -O /dev/null http://localhost:7474
sleep 20
count=1
until cypher-shell --format verbose "SHOW DATABASES" || [ count -gt 6 ]
do
    count=$(( $count + 1 ))
    sleep 10
done

for f in /docker-entrypoint-initdb.d/*.cyp; do
    echo "running cypher ${f}"
    cypher-shell --file $f
done

cypher-shell --format verbose "SHOW CONSTRAINTS"



