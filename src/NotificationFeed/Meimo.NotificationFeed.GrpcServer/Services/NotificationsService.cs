using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Google.Protobuf.WellKnownTypes;

using Grpc.Core;

using Meimo.NotificationFeed.Core;

using Microsoft.Extensions.Logging;

using MongoDB.Bson;

namespace Meimo.NotificationFeed.GrpcServer
{
    public class NotificationsService : Notifications.NotificationsBase
    {
        private readonly ILogger<NotificationsService> _logger;
        private readonly INotificationFeedService _notificationFeedService;

        public NotificationsService(
            ILogger<NotificationsService> logger,
            INotificationFeedService notificationFeedService)
        {
            _logger = logger;
            _notificationFeedService = notificationFeedService;
        }

        public static void ValidateObjectId(string id)
        {
            if (!ObjectId.TryParse(id, out _))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument,
                    $"Notification id must be a mongo object id"));
            }
        }

        public async override Task<CreateNotificationReply> CreateNotification(CreateNotificationRequest request, ServerCallContext context)
        {
            request.Validate();

            NotificationCreatingResultDto notificationCreatingResultDto;

            try
            {
                notificationCreatingResultDto = await _notificationFeedService
                    .CreateNotificationAsync(Guid.Parse(request.PersonId), Guid.Parse(request.EventId), request.Message);
            }
            catch (Exception ex)
            {
                throw new RpcException(new Status(StatusCode.Unknown,
                    ex.InnerException?.Message ?? ex.Message));
            }

            if (notificationCreatingResultDto is null)
            {
                throw new RpcException(new Status(StatusCode.Unknown,
                    "An unknown error occurred while creating a notification"));
            }

            return notificationCreatingResultDto.ToCreateNotificationReply();
        }

        public async override Task<NotificationReply> GetNotification(NotificationLookupModel request, ServerCallContext context)
        {
            if (request?.Id is null)
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument,
                    "Notification Id cannot be null"));
            }

            ValidateObjectId(request.Id);

            var notification = await _notificationFeedService.GetNotificationAsync(request.Id);

            if (notification is null)
            {
                throw new RpcException(new Status(StatusCode.NotFound,
                    "Notification with specified id not found"));
            }

            return notification.ToNotificationReply();
        }

        public async override Task<PaginatedNotificationsReply> GetPaginatedNotifications(PaginatedNotificationsRequest request, ServerCallContext context)
        {
            bool hasParent = !string.IsNullOrWhiteSpace(request.Parent);
            bool hasPageToken = !string.IsNullOrWhiteSpace(request.PageToken);

            if (hasParent) ValidateObjectId(request.Parent);
            if (hasPageToken) ValidateObjectId(request.PageToken);

            if (hasParent && !hasPageToken)
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument,
                    $"No page token specified"));
            }

            if (!Guid.TryParse(request.PersonId, out Guid personId))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument,
                    $"{nameof(request.PersonId)} must be a GUID"));
            }

            var paginatedNotificationsDto = await _notificationFeedService
                .GetPaginatedNotificationsAsync(personId,
                                                request.Parent,
                                                request.PageSize,
                                                request.PageToken,
                                                (SortOrder)request.SortOrder);

            return paginatedNotificationsDto.ToPaginatedNotificationsReply();
        }

        public async override Task<Empty> ModifyNotificationEmailSent(ModifyNotificationEmailSentRequest request, ServerCallContext context)
        {
            await ModifyNotificationFieldAsync(request?.Id, x => x.EmailSent, request?.EmailSent ?? false);

            return new Empty();
        }

        public async override Task<Empty> ModifyNotificationHasBeenRead(ModifyNotificationHasBeenReadRequest request, ServerCallContext context)
        {
            await ModifyNotificationFieldAsync(request?.Id, x => x.HasBeenRead, request?.HasBeenRead ?? false);

            return new Empty();
        }

        public async override Task<Empty> ModifyNotificationPushSent(ModifyNotificationPushSentRequest request, ServerCallContext context)
        {
            await ModifyNotificationFieldAsync(request?.Id, x => x.PushSent, request?.PushSent ?? false);

            return new Empty();
        }

        private async Task ModifyNotificationFieldAsync(string id, Expression<Func<Notification, bool>> field, bool value)
        {
            if (id is null)
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument,
                    "Notification Id cannot be null"));
            }

            ValidateObjectId(id);

            try
            {
                await _notificationFeedService.ModifyNotificationFieldAsync(id, field, value);
            }
            catch (NotificationNotFoundException notFoundException)
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument,
                    notFoundException.Message));
            }
            catch (Exception ex)
            {
                throw new RpcException(new Status(StatusCode.Unknown,
                    ex.InnerException?.Message ?? ex.Message));
            }
        }
    }
}
