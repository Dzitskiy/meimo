﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Meimo.NotificationFeed.Core
{
    public class NotificationCreatingResultDto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public int CreateRetries { get; set; }
    }
}
