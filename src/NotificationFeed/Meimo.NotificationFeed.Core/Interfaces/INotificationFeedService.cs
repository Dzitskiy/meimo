﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Meimo.NotificationFeed.Core
{
    public interface INotificationFeedService
    {
        const int PageSize = 20;

        /// <summary>
        /// Создает новое уведомление
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="eventId"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        Task<NotificationCreatingResultDto> CreateNotificationAsync(Guid personId, Guid eventId, string message);

        /// <summary>
        /// Возвращает уведомление по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown when specified notification id is null</exception>
        Task<Notification> GetNotificationAsync(string id);

        /// <summary>
        /// Возвращает страницу начинающуюся с документа с идентификатором pageToken
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="parent"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageToken"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">
        /// Thrown when specified parent is not null and pageToken is null</exception>
        Task<PaginatedNotificationsDto> GetPaginatedNotificationsAsync(
            Guid personId,
            string parent = null,
            int pageSize = PageSize,
            string pageToken = null,
            SortOrder sortOrder = SortOrder.Ascending);

        /// <summary>
        /// Изменяет значение поля в документе с указанным идентификатором
        /// </summary>
        /// <param name="id"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown when specified notification id or field expression is null</exception>
        /// <exception cref="Meimo.NotificationFeed.Core.NotificationNotFoundException">
        /// Thrown when notification with specified id not found</exception>
        Task ModifyNotificationFieldAsync(string id, Expression<Func<Notification, bool>> field, bool value);
    }
}
