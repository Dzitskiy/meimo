﻿namespace Meimo.NotificationFeed.Infrastructure
{
    public interface INotificationFeedDbSettings
    {
        string CollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
