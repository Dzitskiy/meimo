﻿using System;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;
using Meimo.NotificationFeed.Core;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;
using System.Linq.Expressions;

namespace Meimo.NotificationFeed.Infrastructure
{
    public class NotificationFeedService : INotificationFeedService
    {
        private readonly IMongoCollection<Notification> _notifications;

        public const int DefaultPageSize = 20;

        public NotificationFeedService(INotificationFeedDbSettings settings)
        {
            var conventionPack = new ConventionPack 
            { 
                new CamelCaseElementNameConvention() 
            };

            ConventionRegistry.Register("camelCase", conventionPack, filter => true);

            BsonDefaults.GuidRepresentationMode = GuidRepresentationMode.V3;
            BsonSerializer.RegisterSerializer(new GuidSerializer(GuidRepresentation.Standard));

            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _notifications = database.GetCollection<Notification>(settings.CollectionName);
        }

        public async Task<NotificationCreatingResultDto> CreateNotificationAsync(
            Guid personId, Guid eventId, string message)
        {
            var notification = new Notification()
            {
                PersonId = personId,
                EventId = eventId,
                Message = message,
                CreateRetries = 0,
                HasBeenRead = false,
                PushSent = false,
                EmailSent = false
            };

            bool exists = false;

            try
            {
                await _notifications.InsertOneAsync(notification);
            }
            catch (MongoWriteException e)
            {
                if (e.WriteError.Code == 11000 &&
                    e.WriteError.Message.Contains("personId_1_eventId_1"))
                {
                    exists = true;
                }
            }

            var personFilter = Builders<Notification>.Filter.Eq(x => x.PersonId, personId);
            var eventFilter = Builders<Notification>.Filter.Eq(x => x.EventId, eventId);
            var combineFilters = Builders<Notification>.Filter.And(personFilter, eventFilter);
            var projection = Builders<Notification>.Projection.Include(n => n.CreateRetries);

            var bsonObject = await _notifications
                .Find(combineFilters)
                .Project(projection)
                .FirstOrDefaultAsync();

            NotificationCreatingResultDto result = bsonObject is null
                ? null
                : BsonSerializer.Deserialize<NotificationCreatingResultDto>(bsonObject);

            if (exists && !(result is null))
            {
                var updateDef = Builders<Notification>.Update.Inc(x => x.CreateRetries, 1);

                await _notifications.UpdateOneAsync(x => x.Id == result.Id, updateDef);

                result.CreateRetries++;
            }

            return result;
        }

        public async Task<Notification> GetNotificationAsync(string id)
        {
            if (id is null) throw new ArgumentNullException(nameof(id));

            var notification = await _notifications
                .Find(x => x.Id == id)
                .FirstOrDefaultAsync();

            return notification;
        }

        public async Task<PaginatedNotificationsDto> GetPaginatedNotificationsAsync(
            Guid personId, 
            string parent = null, 
            int pageSize = DefaultPageSize, 
            string pageToken = null,
            SortOrder sortOrder = SortOrder.Ascending)
        {
            if (pageSize < 1) pageSize = DefaultPageSize;

            bool hasParent = !string.IsNullOrWhiteSpace(parent);
            bool hasPageToken = !string.IsNullOrWhiteSpace(pageToken);

            if (hasParent && !hasPageToken)
            {
                throw new ArgumentException($"No page token specified");
            }

            var filter = Builders<Notification>.Filter.Eq(x => x.PersonId, personId);

            if (hasPageToken)
            {
                var pageFilter = sortOrder switch
                {
                    SortOrder.Ascending => Builders<Notification>.Filter.Gte(x => x.Id, pageToken),
                    SortOrder.Descending => Builders<Notification>.Filter.Lte(x => x.Id, pageToken),
                    _ => Builders<Notification>.Filter.Gte(x => x.Id, pageToken)
                };

                filter = Builders<Notification>.Filter.And(filter, pageFilter);
            }

            var sortDefinition = sortOrder switch
            {
                SortOrder.Ascending => Builders<Notification>.Sort.Ascending(x => x.Id),
                SortOrder.Descending => Builders<Notification>.Sort.Descending(x => x.Id),
                _ => Builders<Notification>.Sort.Ascending(x => x.Id)
            };

            var notifications = await _notifications
                .Find(filter)
                .Sort(sortDefinition)
                .Limit(pageSize + 1)
                .ToListAsync();

            string nextPageToken = string.Empty;

            if (notifications.Count > pageSize)
            {
                nextPageToken = notifications[notifications.Count - 1].Id;
                notifications.RemoveAt(notifications.Count - 1);
            }

            var result = new PaginatedNotificationsDto(notifications, nextPageToken);

            return result;
        }

        public async Task ModifyNotificationFieldAsync(
            string id, Expression<Func<Notification, bool>> field, bool value)
        {
            if (id is null) throw new ArgumentNullException(nameof(id));

            if (field is null) throw new ArgumentNullException(nameof(field));

            var filter = Builders<Notification>.Filter.Eq(x => x.Id, id);

            var update = Builders<Notification>.Update.Set(field, value);

            var updateResult = await _notifications.UpdateOneAsync(filter, update);

            if (!(updateResult.IsAcknowledged && updateResult.MatchedCount > 0))
            {
                throw new NotificationNotFoundException(
                    $"Notification with id = {id} not found");
            }
        }
    }
}
