version: '3.8'
services:

# --------------- Common Services ---------------

  #Db
  meimo-psqldb:
    image: registry.gitlab.com/oaelgin/meimo/database/psqldb
    container_name: dc-meimo-psqldb
    restart: always
    ports:
      - 35432:5432
    environment:
      - POSTGRES_PASSWORD=meimo
      - PGDATA=/var/lib/postgresql/data
    volumes:
      - dc-meimo-psqldb-data:/var/lib/postgresql/data

  #PgAdmin
  pgadmin:
    image: dpage/pgadmin4
    container_name: dc-pgadmin
    restart: always
    environment:
      PGADMIN_DEFAULT_EMAIL: pgadmin@meimo.fun
      PGADMIN_DEFAULT_PASSWORD: meimo
      PGADMIN_LISTEN_PORT: 80
    ports:
      - 35050:80
    volumes:
      - dc-pgadmin-data:/var/lib/pgadmin
    links:
      - "meimo-psqldb:pgsql-server"

  #Neo4j
  meimo-neo4j:
    image: meimo/infrastructure-neo4j
    container_name: dc-meimo-neo4j
    restart: always
    ports:
      - 37474:7474
      - 37687:7687
    environment:
      - NEO4J_AUTH=neo4j/meimo
      - NEO4J_USERNAME=neo4j
      - NEO4J_PASSWORD=meimo
    volumes:
      - dc-meimo-neo4j-logs:/logs
      - dc-meimo-neo4j-data:/data

  #Zookeeper
  zookeeper:
    image: confluentinc/cp-zookeeper:6.1.1
    hostname: zookeeper
    container_name: dc-zookeeper
    ports:
      - "32181:2181"
    environment:
      ZOOKEEPER_CLIENT_PORT: 2181
      ZOOKEEPER_TICK_TIME: 2000
    volumes:
      - dc-zookeeper-data:/var/lib/zookeeper/data
      - dc-zookeeper-log:/var/lib/zookeeper/log
      - dc-zookeeper-secrets:/etc/zookeeper/secrets

  #Kafka
  #Clients within the Docker network connect using bootstrap server broker:9092
  #The Docker host machine clients connect using bootstrap server localhost:29092
  #External clients connect using bootstrap server meimo.fun:39092
  broker:
    image: confluentinc/cp-kafka:6.1.1
    hostname: broker
    container_name: dc-broker
    depends_on:
      zookeeper:
        condition: service_started
    ports:
      - "29092:29092"
      - "39092:39092"
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_ZOOKEEPER_CONNECT: 'zookeeper:2181'
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://broker:9092,PLAINTEXT_HOST://localhost:29092,PLAINTEXT_OUTSIDE://meimo.fun:39092
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: PLAINTEXT:PLAINTEXT,PLAINTEXT_HOST:PLAINTEXT,PLAINTEXT_OUTSIDE:PLAINTEXT
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
      KAFKA_GROUP_INITIAL_REBALANCE_DELAY_MS: 0
    volumes:
      - dc-kafka-data:/var/lib/kafka/data
      - dc-kafka-secrets:/etc/kafka/secrets

  # https://github.com/confluentinc/examples/blob/5.1.1-post/microservices-orders/docker-compose.yml#L182-L215
  # This "container" is a workaround to pre-create topics
  kafka-setup:
    image: confluentinc/cp-kafka:6.1.1
    hostname: kafka-setup
    container_name: dc-kafka-setup
    depends_on:
      broker:
        condition: service_started
    command: "bash -c 'echo Waiting for Kafka to be ready... && \
                       sleep 25 && \
                       cub kafka-ready -b broker:9092 1 20 && \
                       kafka-topics --create --if-not-exists --zookeeper zookeeper:2181 --partitions 1 --replication-factor 1 --topic replication.person.profile && \
                       kafka-topics --create --if-not-exists --zookeeper zookeeper:2181 --partitions 3 --replication-factor 1 --topic tracking.friends.event && \
                       kafka-topics --create --if-not-exists --zookeeper zookeeper:2181 --partitions 3 --replication-factor 1 --topic tracking.club.event && \
                       kafka-topics --create --if-not-exists --zookeeper zookeeper:2181 --partitions 1 --replication-factor 1 --topic user.test.data'"
    environment:
      # The following settings are listed here only to satisfy the image's requirements.
      # We override the image's `command` anyways, hence this container will not start a broker.
      KAFKA_BROKER_ID: ignored
      KAFKA_ZOOKEEPER_CONNECT: ignored
    volumes:
      - dc-kafka-setup-data:/var/lib/kafka/data
      - dc-kafka-setup-secrets:/etc/kafka/secrets

# --------------- Account Services ---------------

  #AccountApi
  meimo-person-account-api:
    image: registry.gitlab.com/oaelgin/meimo/person/account-webapi
    container_name: 'dc-meimo-person-account-api'
    restart: always
    ports:
      - 35001:5000
    environment:
      - "ConnectionStrings__DefaultConnection=Host=meimo-psqldb;Port=5432;Database=meimo;Username=postgres;Password=meimo;Pooling=true"
      - "ConnectionStrings__EmailNotifier=http://meimo-emailnotifier-api:5000/"
      - "EmailConfig__FromAddress=noreply@meimo.fun"
      - "EmailConfig__TemplateId=not set"
      - "EmailActivationConfig__ActivationUrl=http://meimo-person-account-api:5000/activate/"
    depends_on:
      meimo-psqldb:
        condition: service_started

# --------------- Profile Services ---------------

  #ProfileGrpc
  meimo-person-profile-grpc:
    image: registry.gitlab.com/oaelgin/meimo/person/profile-grpc
    container_name: 'dc-meimo-person-profile-grpc'
    restart: always
    ports:
      - 35002:5000
      - 35012:5002
    environment:
      - "Kestrel__Endpoints__Http1Api__Url=http://*:5000"
      - "Kestrel__Endpoints__Http1Api__Protocols=Http1"
      - "Kestrel__Endpoints__Http2Grpc__Url=http://*:5002"
      - "Kestrel__Endpoints__Http2Grpc__Protocols=Http2"
      - "ConnectionStrings__DefaultConnection=Host=meimo-psqldb;Port=5432;Database=meimo;Username=postgres;Password=meimo;Pooling=true"
      - "Kafka__ProducerSettings__BootstrapServers=broker:9092"
      - "Kafka__ReplicationProfileTopic=replication.person.profile"
    depends_on:
      meimo-psqldb:
        condition: service_started
      broker:
        condition: service_started

# --------------- Friends Services ---------------

  #FriendsGraphQL
  meimo-friends-graphql:
    image: registry.gitlab.com/oaelgin/meimo/friends/graphql
    container_name: 'dc-meimo-friends-graphql'
    restart: always
    ports:
      - 35004:5000
    environment:
      - "FriendsDbSettings__ConnectionString=bolt://meimo-neo4j:7687"
      - "FriendsDbSettings__User=neo4j"
      - "FriendsDbSettings__Password=meimo"
      - "FriendsDbSettings__DatabaseName=neo4j"
      - "ConnectionStrings__ProfileGrpcServer=http://meimo-person-profile-grpc:5002"
      - "Kafka__ProducerSettings__BootstrapServers=broker:9092"
      - "Kafka__TrackingEventTopic=tracking.friends.event"
    depends_on:
      meimo-neo4j:
        condition: service_started
      broker:
        condition: service_started
      meimo-person-profile-grpc:
        condition: service_started

  #FriendsProfileReplicationWorker
  meimo-friends-profile-replication:
    image: registry.gitlab.com/oaelgin/meimo/friends/profile-replication-worker
    container_name: 'dc-meimo-friends-profile-replication'
    restart: always
    environment:
      - "FriendsDbSettings__ConnectionString=bolt://meimo-neo4j:7687"
      - "FriendsDbSettings__User=neo4j"
      - "FriendsDbSettings__Password=meimo"
      - "FriendsDbSettings__DatabaseName=neo4j"
      - "Kafka__ConsumerSettings__BootstrapServers=broker:9092"
      - "Kafka__ConsumerSettings__GroupId=replication-person-group"
      - "Kafka__ConsumerSettings__AutoOffsetReset=Earliest"
      - "Kafka__ReplicationProfileTopic=replication.person.profile"
    depends_on:
      meimo-neo4j:
        condition: service_started
      broker:
        condition: service_started

# --------------- Volumes ---------------

volumes:
  dc-meimo-psqldb-data:
  dc-pgadmin-data:
  dc-meimo-neo4j-logs:
  dc-meimo-neo4j-data:
  dc-zookeeper-data:
  dc-zookeeper-log:
  dc-zookeeper-secrets:
  dc-kafka-data:
  dc-kafka-secrets:
  dc-kafka-setup-data:
  dc-kafka-setup-secrets:
